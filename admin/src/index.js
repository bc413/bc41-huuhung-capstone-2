import {renderPhone,onSuccess,layThongTinTuForm,upThongTinLenInput,onFall} from "./controllert.js"
import {kietTraNhap,kiemTraSo,kiemTraTrung,checkDoDdai} from "./validation.js"

let BASE_URL = "https://63da51d8b28a3148f684171b.mockapi.io/phone"


let fetPhoneList = ()=>{
    axios({
        url: `${BASE_URL}`,
        method: 'GET',
    })
    .then((ress)=>{
        renderPhone(ress.data)
    })
    .catch((err)=>{
        console.log(err)
    });
};
fetPhoneList()


let xoaPhone = (id)=>{
    axios({
        url: `${BASE_URL}/${id}`,
        method: "DELETE"
    })
    .then((ress)=>{
        console.log(ress)
        fetPhoneList()
        onSuccess()
    })
    .catch((err)=>{
        console.log(err)
    })
};
window.xoaPhone=xoaPhone;


let themPhone = () =>{
    let tenSP= layThongTinTuForm().name;
    let moTa = layThongTinTuForm().desc
    let name = layThongTinTuForm().name
    let kq1 = kietTraNhap("#maSp","#tbSp",0) && kiemTraSo("#maSp","#tbSp", 0);
    let kp2 = kietTraNhap("#TenSP","#tbTen",1) && checkDoDdai(name,5,50,"#tbTen","Tên sản phẩm") && kiemTraTrung(BASE_URL,tenSP);
    let kp3 = kietTraNhap("#GiaSP","#tbGia",2) && kiemTraSo("#GiaSP","#tbGia",0);
    let kp4 = kietTraNhap("#HinhSP","#tbImg",3);
    let kp5 = kietTraNhap("#MoTa","#tbDes",4) && checkDoDdai(moTa,5,200,"#tbDes","Mô tả");
    if(kq1 && kp2 && kp3 && kp4 && kp5){
        axios({
            url: `${BASE_URL}`,
            method: "POST",
            data: layThongTinTuForm(),
        })
        .then((ress)=>{
            console.log(ress)
            fetPhoneList()
            onSuccess()
            document.querySelector(".capNhat").style.display = "block"
            $('#exampleModal').modal('toggle')
            document.querySelector("#maSp").value = "";
            document.querySelector("#TenSP").value = "";
            document.querySelector("#GiaSP").value = "";
            document.querySelector("#HinhSP").value = "";
            document.querySelector("#MoTa").value = "";
        })
        .catch((err)=>{
            console.log(err)
            onFall()
        })
    }
};
window.themPhone=themPhone;

let showPhone = (id) =>{
    document.querySelector(".addPhone").style.display = "none"
    axios({
        url: `${BASE_URL}/${id}`,
        method: "GET"
    })
    .then((ress)=>{
        console.log(ress)
        upThongTinLenInput(ress.data)
        $('#exampleModal').modal('show')
        document.getElementById("maSp").disabled = true;
    })
    .catch((err)=>{
        console.log(err)
    })
};
window.showPhone = showPhone;
    
let capNhat = () =>{
    let data = layThongTinTuForm();
    let tenSP= data.name;
    let moTa = data.desc
    let name = data.name
    let kp2 = kietTraNhap("#TenSP","#tbTen",1) && checkDoDdai(name,5,50,"#tbTen","Tên sản phẩm") && kiemTraTrung(BASE_URL,tenSP);
    let kp3 = kietTraNhap("#GiaSP","#tbGia",2) && kiemTraSo("#GiaSP","#tbGia",0);
    let kp4 = kietTraNhap("#HinhSP","#tbImg",3);
    let kp5 = kietTraNhap("#MoTa","#tbDes",4) && checkDoDdai(moTa,5,200,"#tbDes","Mô tả");
    if(kp2 && kp3 && kp4 && kp5){
        axios({
            url: `${BASE_URL}/${data.maSp}`,
            method: "PUT",
            data: data,
        })
        .then((ress)=>{
            console.log(ress)
            fetPhoneList()
            onSuccess()
            document.querySelector(".addPhone").style.display = "block"
            $('#exampleModal').modal('hide')
            document.getElementById("maSp").disabled = false;
            document.querySelector("#maSp").value = "";
            document.querySelector("#TenSP").value = "";
            document.querySelector("#GiaSP").value = "";
            document.querySelector("#HinhSP").value = "";
            document.querySelector("#MoTa").value = "";
        })
        .catch((err)=>{
            console.log(err)
        })
    }
};
window.capNhat = capNhat;

let clickOpenForm = ()=>{
    let click = document.getElementById("clicks");
    click.addEventListener("click",()=>{
        document.querySelector(".capNhat").style.display = "none"
        $('#exampleModal').modal('show')
    })
    
}
clickOpenForm()
