export let renderPhone = (arr) =>{
    let contentHTML = "";
    arr.forEach((item)=>{
        let content = `
        <tr>
            <td>${item.id}</td>
            <td>${item.name}</td>
            <td>$${item.price}</td>
            <td><img style="width: 80px;" src="${item.img}" alt="" /> </td>
            <td>${item.desc}</td>
            <td>
                <div class="d-flex align-items-center">
                    <button onclick="xoaPhone(${item.id})" class="btn btn-danger mr-2" >Xóa</button>
                    <button onclick="showPhone(${item.id})" class="btn btn-primary">Sửa</button>
                </div>
            </td>
        </tr>
        `
        contentHTML += content;
    });
    document.querySelector("#tblDanhSachSP").innerHTML = contentHTML;
}


export let onSuccess = () =>{
    Toastify({
        text: "Thành công...",
        duration: 3000
        }).showToast();
}
export let onFall = () =>{
    Toastify({
        text: "Thất bại...",
        duration: 3000
        }).showToast();
}


export let layThongTinTuForm = () =>{
    let maSp = document.querySelector("#maSp").value;
    let name = document.querySelector("#TenSP").value;
    let price = document.querySelector("#GiaSP").value;
    let img = document.querySelector("#HinhSP").value;
    let desc = document.querySelector("#MoTa").value;

    let data = {
        maSp,
        name,
        price,
        img,
        desc,
    }
    return data;
}


export let upThongTinLenInput = (arr) =>{
    document.querySelector("#maSp").value = arr.id;
    document.querySelector("#TenSP").value = arr.name;
    document.querySelector("#GiaSP").value = arr.price;
    document.querySelector("#HinhSP").value = arr.img;
    document.querySelector("#MoTa").value = arr.desc;
};