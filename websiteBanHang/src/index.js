axios({
    url: "https://63da51d8b28a3148f684171b.mockapi.io/phone",
    method: "GET",
})
.then((res)=>{
    renderPhone(res.data)
})
.catch((err)=>{
    console.log(err)
})


let renderPhone = (arr) =>{
    let contentHTML = "";
    arr.forEach((item)=>{
        let content = `
        <div class="item">
        <div class="cart">
            <div class="img">
                <img src="${item.img}" alt="">
            </div>
            <div class="infor">
                <h3 class = "name">${item.name}</h3>
                <p class="desc">
                    <span>Desc:</span> ${item.desc}
                </p>
                <p class="screen">
                    <span>Screen:</span> ${item.screen}
                </p>
                <p class="backCamera">
                    <span>BackCamera:</span> ${item.backCamera}
                </p>
                <p class="frontCamera">
                    <span>FrontCamera:</span> ${item.frontCamera}
                </p>
                <p class="price">
                    <span>Price:</span> $${item.price}
                </p>
            </div>
        </div>
    </div>
</div>`
    contentHTML += content
    });
    document.querySelector(".phone__content").innerHTML = contentHTML
}